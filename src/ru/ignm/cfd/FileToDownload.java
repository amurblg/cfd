package ru.ignm.cfd;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class FileToDownload {
    private String iniFile;
    private Properties props = new Properties();

    public FileToDownload(String iniFile) throws IOException {
        this.iniFile = iniFile;

        CheckINICorrectness();
        ReadProperties();
    }

    private void ReadProperties() throws IOException {
        InputStreamReader isr = new InputStreamReader(new FileInputStream(iniFile), "UTF-8");
        props.load(isr);
    }

    private void SaveProperties() throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(iniFile), "UTF-8");
        props.store(osw, null);
    }

    //Create INI file if not exists. If exists - check its validity.
    private void CheckINICorrectness() throws IOException {
        File f = new File("params.ini");
        if (!f.exists()) {
            FileOutputStream fos = new FileOutputStream("params.ini");

            props.setProperty("url", "http://83.169.225.115/data/data.rar");
            //OS specific params:
            if (!System.getProperty("os.name").toLowerCase().contains("windows")) {
                props.setProperty("tempDir", "/home/john/");
                props.setProperty("dir2Unpack", "/home/john/test/");
                props.setProperty("path2Archiver", "/usr/bin/rar");
            } else {
                props.setProperty("tempDir", "C:\\WINDOWS\\TEMP");
                props.setProperty("dir2Unpack", "C:\\UNPACK\\Тестовая папка\\d2\\");
                props.setProperty("path2Archiver", "C:\\PROGRAM FILES\\WinRAR\\winrar.exe");
            }

            props.store(fos, null);
            fos.close();
        } else {//INI file exists, but some properties may be missed - check 'em all
            ReadProperties();

            if (props.getProperty("url") == null || props.getProperty("url").length() < 10)
                props.setProperty("url", "http://83.169.225.115/data/data.rar");
            //OS specific params:
            if (!System.getProperty("os.name").toLowerCase().contains("windows")) {
                if (props.getProperty("tempDir") == null || props.getProperty("tempDir").length() < 3) {
                    props.setProperty("tempDir", "/home/john/");
                }
                if (props.getProperty("dir2Unpack") == null || props.getProperty("dir2Unpack").length() < 3) {
                    props.setProperty("dir2Unpack", "/home/john/test/");
                }
                if (!props.getProperty("dir2Unpack").endsWith(File.separator))//Otherwise rar just do not works, says there's no files to extract!
                    props.setProperty("dir2Unpack", props.get("dir2Unpack") + File.separator);
                if (props.getProperty("path2Archiver") == null || props.getProperty("path2Archiver").length() < 3) {
                    props.setProperty("path2Archiver", "/usr/bin/rar");
                }
            } else {
                if (props.getProperty("tempDir") == null || props.getProperty("tempDir").length() < 3) {
                    props.setProperty("tempDir", "C:\\WINDOWS\\TEMP");
                }
                if (props.getProperty("dir2Unpack") == null || props.getProperty("dir2Unpack").length() < 3) {
                    props.setProperty("dir2Unpack", "C:\\UNPACK");
                }
                if (!props.getProperty("dir2Unpack").endsWith(File.separator))//Otherwise rar just do not works, says there's no files to extract!
                    props.setProperty("dir2Unpack", props.get("dir2Unpack") + File.separator);
                if (props.getProperty("path2Archiver") == null || props.getProperty("path2Archiver").length() < 3) {
                    props.setProperty("path2Archiver", "C:\\PROGRAM FILES\\WinRAR\\winrar.exe");
                }
            }

            SaveProperties();
        }
    }

    public void Download() throws IOException, InterruptedException {
        //construct full path for file to download (to the temp dir)
        Path filePath = Paths.get(new URL(props.getProperty("url")).getFile());//getFile returns "data/data.rar" part, not "data.rar"! (when url is "http://83.169.225.115/data/data.rar")
        Path p = Paths.get(props.getProperty("tempDir"), filePath.getFileName().toString());
        File dest = new File(p.toString());
        URL url = new URL(props.getProperty("url"));

        //download file. Default timeout - 30s
        FileUtils.copyURLToFile(url, dest, 30000, 30000);
    }

    public int Unpack() throws IOException, InterruptedException {
        Path filePath = Paths.get(new URL(props.getProperty("url")).getFile());
        Path path2Arc = Paths.get(props.getProperty("tempDir"), filePath.getFileName().toString());
        //"x -o+" switches in rar command line - unpack with overwrite!
        ProcessBuilder pb = new ProcessBuilder(props.getProperty("path2Archiver"), "x", "-o+", path2Arc.toString(), props.getProperty("dir2Unpack"));
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);

        Process process = pb.start();

        return process.waitFor();
    }
}
