package ru.ignm.cfd;

public class Main {

    public static void main(String[] args) {
        String iniFile = "params.ini";
        if (args.length > 0)
            iniFile = args[0];

        FileToDownload ftd;
        try {
            ftd = new FileToDownload(iniFile);
            ftd.Download();
            Thread.sleep(500);
            ftd.Unpack();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }
}
