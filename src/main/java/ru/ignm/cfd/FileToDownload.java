package ru.ignm.cfd;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import java.util.logging.*;
import java.util.stream.Collectors;

public class FileToDownload {
    private String iniFile;
    private Properties props = new Properties();

    private Logger logger = Logger.getLogger("CFD");
    private Handler consoleHandler;//make it class-level in order to set proper encoding

    public FileToDownload(String iniFile) throws IOException {
        this.iniFile = iniFile;

        CheckINICorrectness();
        ReadProperties();

        Handler fileHandler = new FileHandler("cfd.log", true);
        consoleHandler = new ConsoleHandler();
        if (System.getProperty("os.name").toLowerCase().contains("windows")) {//otherwise in windows console we'll get mojibake
            consoleHandler.setEncoding("cp866");
        }

        logger.addHandler(fileHandler);
        logger.addHandler(consoleHandler);
        logger.setUseParentHandlers(false);
        SimpleFormatter formatter = new SimpleFormatter();
        fileHandler.setFormatter(formatter);

    }

    private void ReadProperties() throws IOException {
        InputStreamReader isr = new InputStreamReader(new FileInputStream(iniFile), "UTF-8");
        props.load(isr);
    }

    private void SaveProperties() throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(iniFile), "UTF-8");
        props.store(osw, null);
    }

    //Create INI file if not exists. If exists - check its validity.
    private void CheckINICorrectness() throws IOException {
        File f = new File("params.ini");
        if (!f.exists()) {
            FileOutputStream fos = new FileOutputStream("params.ini");

            props.setProperty("url", "http://83.169.225.115/data/data.rar");
            props.setProperty("logEnabled", "true");
            props.setProperty("logLevel", "INFO");
            logger.setLevel(Level.INFO);

            props.setProperty("clearTargetDirBeforeUnpack", "false");
            //OS specific params:
            if (!System.getProperty("os.name").toLowerCase().contains("windows")) {
                props.setProperty("tempDir", "/home/john/");
                props.setProperty("dir2Unpack", "/home/john/test/");
                props.setProperty("path2Archiver", "/usr/bin/rar");
            } else {
                props.setProperty("tempDir", "C:\\WINDOWS\\TEMP");
                props.setProperty("dir2Unpack", "C:\\UNPACK\\Тестовая папка\\d2\\");
                props.setProperty("path2Archiver", "C:\\PROGRAM FILES\\WinRAR\\rar.exe");
            }

            props.store(fos, null);
            fos.close();
        } else {//INI file exists, but some properties may be missed - check 'em all
            ReadProperties();

            if (props.getProperty("url") == null || props.getProperty("url").length() < 10)
                props.setProperty("url", "http://83.169.225.115/data/data.rar");

            if (props.getProperty("logEnabled") == null)
                props.setProperty("logEnabled", "true");

            if (props.getProperty("logLevel") == null) {
                props.setProperty("logLevel", "INFO");
            } else {
                if ("SEVERE".equals(props.getProperty("logLevel")))
                    logger.setLevel(Level.SEVERE);
                else if ("WARNING".equals(props.getProperty("logLevel")))
                    logger.setLevel(Level.WARNING);
                else logger.setLevel(Level.INFO);
            }

            if (props.getProperty("clearTargetDirBeforeUnpack") == null)
                props.setProperty("clearTargetDirBeforeUnpack", "false");
            //OS specific params:
            if (!System.getProperty("os.name").toLowerCase().contains("windows")) {
                if (props.getProperty("tempDir") == null || props.getProperty("tempDir").length() < 3) {
                    props.setProperty("tempDir", "/home/john/");
                }
                if (props.getProperty("dir2Unpack") == null || props.getProperty("dir2Unpack").length() < 3) {
                    props.setProperty("dir2Unpack", "/home/john/test/");
                }
                if (!props.getProperty("dir2Unpack").endsWith(File.separator))//Otherwise rar just do not works, says there's no files to extract!
                    props.setProperty("dir2Unpack", props.get("dir2Unpack") + File.separator);
                if (props.getProperty("path2Archiver") == null || props.getProperty("path2Archiver").length() < 3) {
                    props.setProperty("path2Archiver", "/usr/bin/rar");
                }
            } else {
                if (props.getProperty("tempDir") == null || props.getProperty("tempDir").length() < 3) {
                    props.setProperty("tempDir", "C:\\WINDOWS\\TEMP");
                }
                if (props.getProperty("dir2Unpack") == null || props.getProperty("dir2Unpack").length() < 3) {
                    props.setProperty("dir2Unpack", "C:\\UNPACK");
                }
                if (!props.getProperty("dir2Unpack").endsWith(File.separator))//Otherwise rar just do not works, says there's no files to extract!
                    props.setProperty("dir2Unpack", props.get("dir2Unpack") + File.separator);
                if (props.getProperty("path2Archiver") == null || props.getProperty("path2Archiver").length() < 3) {
                    props.setProperty("path2Archiver", "C:\\PROGRAM FILES\\WinRAR\\rar.exe");
                }
            }

            SaveProperties();
        }
    }

    public void Download() throws IOException, InterruptedException {
        logMessage("--- Начало процесса загрузки файла ---", Level.INFO);
        //construct full path for file to download (to the temp dir)
        Path filePath = Paths.get(new URL(props.getProperty("url")).getFile());//getFile returns "data/data.rar" part, not "data.rar"! (when url is "http://83.169.225.115/data/data.rar")
        Path p = Paths.get(props.getProperty("tempDir"), filePath.getFileName().toString());
        File dest = new File(p.toString());
        URL url = new URL(props.getProperty("url"));
        logMessage("\tТекущий url для скачивания: " + url.toString(), Level.INFO);
        logMessage("\tВременный путь для сохранения скачанного файла: " + p.toString(), Level.INFO);
        logMessage("\tЗапускаем скачивание...", Level.INFO);

        //download file. Default timeout - 30s
        FileUtils.copyURLToFile(url, dest, 30000, 30000);
        logMessage("--- Файл загружен! ---", Level.INFO);
    }

    private boolean TestArchive(Path path2Arc) throws IOException, InterruptedException {
        logMessage("--- Начало процесса тестирования скачанного архива ---", Level.INFO);

        ProcessBuilder pb = new ProcessBuilder(props.getProperty("path2Archiver"), "t", path2Arc.toString());
        logMessage("\tПараметры строки запуска: " + pb.command().toString(), Level.INFO);
        logMessage("\tЗапускаем тестирование. Вывод процесса:", Level.INFO);
        Process process = pb.start();
        int exitValue;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            logMessage(br.lines().collect(Collectors.joining("\n")), Level.INFO);
        }

        exitValue = process.waitFor();
        logMessage("\tКод выхода процесса: " + exitValue, Level.INFO);
        logMessage("--- Тестирование архмва завершено ---", Level.INFO);

        return (exitValue == 0);
    }

    public int Unpack() throws IOException, InterruptedException {
        logMessage("--- Начало процесса распаковки архива ---", Level.INFO);

        Path filePath = Paths.get(new URL(props.getProperty("url")).getFile());
        Path path2Arc = Paths.get(props.getProperty("tempDir"), filePath.getFileName().toString());

        boolean startUnpack = true;
        if (Boolean.valueOf(props.getProperty("clearTargetDirBeforeUnpack"))) {//Then we need to test archive first
            startUnpack = TestArchive(path2Arc);
            if (startUnpack) {
                logMessage("\tАрхив корректен, очищаем целевой каталог...", Level.INFO);
                try {
                    FileUtils.cleanDirectory(new File(props.getProperty("dir2Unpack")));
                } catch (Exception e) {
                    logMessage("!!! ОШИБКА ОЧИСТКИ КАТАЛОГА: " + e.getMessage(), Level.SEVERE);
                }
                logMessage("\tКаталог очищен.", Level.INFO);
            }
        } else {
            logMessage("Параметр \"Очищать каталог перед распаковкой\" не установлен - распаковываем поверх...", Level.INFO);
        }

        if (startUnpack) {
            //"x -o+" switches in rar command line - unpack with overwrite!
            ProcessBuilder pb = new ProcessBuilder(props.getProperty("path2Archiver"), "x", "-o+", path2Arc.toString(), props.getProperty("dir2Unpack"));

            logMessage("\tПараметры команды распаковки: " + pb.command().toString(), Level.INFO);
            Process process = pb.start();
            logMessage("\tВывод команды распаковки:", Level.INFO);
            try (BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                logMessage(br.lines().collect(Collectors.joining("\n")), Level.INFO);
            }
            int exitValue = process.waitFor();
            logMessage("\tКод выхода процесса: " + exitValue, Level.INFO);
            logMessage("--- Распаковка завершена ---", Level.INFO);

        } else {
            logMessage("!!! ОШИБКА, РАСПАКОВКА ОТМЕНЕНА!", Level.SEVERE);
        }

        return -1;
    }

    private void logMessage(String msg, Level logLevel) {
        if (logLevel == Level.WARNING) {
            logger.warning(msg);
        } else if (logLevel == Level.SEVERE) {
            logger.severe(msg);
        } else {
            logger.info(msg);
        }
    }
}
